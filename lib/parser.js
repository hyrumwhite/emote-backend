exports.getTags = function(content) {
  var hashtagRegex = /(?:\s|\A|^)[##]+([A-Za-z0-9-_]+)/g;
  var hashTags = content.match(hashtagRegex);
  if(hashTags !== null) {
    for(var i = 0; i < hashTags.length; i++) {
      hashTags[i] = hashTags[i].trim();
    }
  }
  return hashTags === null ? [] : hashTags;
};
