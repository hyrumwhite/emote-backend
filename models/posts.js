/* global exports */
var emotions = require('./emotions').emotions;
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log('opened successfully');
});
var postSchema = mongoose.Schema({
  body: String,
  hashtags: [String],
  comments: {},
  commentCount: Number,
  reactions: {
    type: {}, 
    default:emotions
  },
  flagged: Boolean
}, {timestamps:true});
var Post = mongoose.model('Post', postSchema);
var pools = {};
function broadcastToPool(type, response, postId) {
  var toDelete = [];
  for(var i = 0; i < pools[type][postId].length; i++) {
    try {
      pools[type][postId][i].send(response);
    }
    catch(e) {
      toDelete.push(i);
    }
  }
  for(var i = 0; i < toDelete.length; i++) {
    pools[type][postId].splice(toDelete[i],1);
  }
}
exports.search = function(req, ws) {
  var tag = req.tag;
  var cbid = req.cbid;
  var limit = req.limit;
  console.log(tag, limit);
  var query = tag === undefined || tag === '' ? {} : {hashtags: tag};
  Post.find(query).sort({$natural:-1}).limit(limit).skip(0).exec(function(error, posts) {
    if(error) {
      return ws.send({error:500, message:"An error occurred while searching for a tag"});
    }
    console.log(posts);
    ws.send({cbid:cbid, data:posts});
  });
};
exports.getNextSet = function(req, ws) {
  var tag = req.tag;
  var cbid = req.cbid;
  var pageIndex = req.page;
  var query = tag === undefined || tag === '' ? {} : {hashtags: tag};
  Post.find(query).sort({$natural:-1}).limit(20).skip(pageIndex).exec(function(err, posts) {
    if(err) {
      return ws.send({error:500, message:"An error occurred while searching for a tag"});
    }
    ws.send({cbid:cbid, data:posts});
  });
};
//------------------------------------------------------------------------------
exports.enterPool = function(req, ws) {
  var cbid = req.cbid;
  var postId = req.postId;
  var type = req.type;
  pools[type] || (pools[type] = {});
  pools[type][postId] || (pools[type][postId] = []);
  var poolId = pools[type][postId].push(ws);
  var response = {cbid: cbid, data: {poolId:poolId}};
  ws.send(response);
};
//------------------------------------------------------------------------------
exports.leavePool = function(req, ws) {//todo better clean up
  var cbid = req.cbid;
  var postId = req.postId;
  var poolId = req.poolId;
  var type = req.type;
  try {
    pools[type][postId].splice(poolId, 1);
    ws.send({cbid:cbid, data: {success:"true", message:"success"}});
  }
  catch(e) {
    ws.send({cbid:cbid, data: {error:"500", message:"Failed to remove item from pool"}});
  }
};
//------------------------------------------------------------------------------
exports.likeComment = function(req, res) {
  var cbid = req.cbid;
  var postId = req.postId;
  var commentId = req.commentId;
  var updateObject = {};
  updateObject['comments.' + commentId + '.likes'] = 1;
  updateObject = {$inc: updateObject};
  Post.update({'_id': postId}, updateObject, function(err, number) {
    if(err || number.n < 1) {
      var errorMessage = {};
      errorMessage.error = 500;
      errorMessage.message = 'Failed to load doc after comment update';
      errorMessage.cbid = cbid;
      res.send(errorMessage); 
    }
    else {
      var response = {};
      response.cbid = cbid;
      response.broadcast = 'commentLiked';
      response.data = {};
      response.data.commentId = commentId;
      response.data.postId = postId;
      broadcastToPool('comments', response, postId);
    }
  });
};
exports.addComment = function(req, res) {
  var cbid = req.cbid;
  var postId = req.postId;
  Post.findOne({_id: req.postId}, function(err, post) {
    var commentId = post.commentCount;
    var comment = {body: req.body, likes: 0};
    var updateObject = {};
    updateObject['comments.' + commentId] = comment;
    Post.update({_id: req.postId}, {$set: updateObject, $inc:{commentCount: 1}}, function(err, number) {
      Post.findOne({'_id':postId}, function(err, post) {
        if(err) {
          res.send({cbid: cbid, error: 500, message: "Failed to load doc after comment update"});
        }
        else {
          res.send({cbid:cbid,data:{message:'success!'}});
          var response = {cbid: cbid, broadcast:'commentAdded', data:{comments: post.comments, commentCount:post.commentCount, postId: postId}};
          broadcastToPool('comments', response, postId);
        }
      });
    });
  });
};
exports.addReaction = function(req, res) {
  var cbid = req.cbid;
  var postId = req.postId;
  var emotion = req.emotion;
  var updateObject = {};
  updateObject['reactions.' + emotion] = 1;
  console.log("REACTING!");
  Post.update({_id: req.postId}, {$inc:updateObject}, function(err, number) {
    if(err) {
      return res.send({cbid: cbid, error: 500, message: "Failed to update doc"});  
    }
    res.send({cbid:cbid, data:{message:'success!'}});
    Post.findOne({'_id':postId}, function(err, post) {
      if(err) {
        return res.send({cbid: cbid, error: 500, message: "Failed to load doc after reaction update"});
      }
      var emotionCount = {};
      emotionCount[emotion] = post.reactions[emotion];
      var response = {cbid: cbid, broadcast:'reactionAdded', data:{emotion: emotionCount, postId: postId}};
      broadcastToPool('reactions', response, postId);
    });
    });
};
exports.newPost = function(document) {
  return new Post(document);
};
exports.pushItem = function(id, key, value, callback) {
  var query = {_id: id};
  var updateObject = {$push: {}};
  if(key === 'reactions') {
    updateObject.$inc = {reactionCount: 1};
  }
  updateObject.$push[key] = value;
  Post.update(query, updateObject, callback);
};
exports.getAll = function(callback) {
  Post.find({}).limit(20).skip(0).exec(callback);
};
exports.getByQuery = function(query, callback) {
  Post.find(query, callback);
};