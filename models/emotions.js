exports.emotions = function() {
  return {
    happy: 0,
    sad: 0,
    upset: 0,
    verysad: 0,
    veryhappy: 0,
    angry: 0,
    scared: 0,
    confused: 0,
    tired: 0
  };
};
exports.emoticons = function() {
  return {
    '>:(':'angry',
    "T.T":'verysad',
    '=_=':'tired',
    "@.@":"confused",
    ":'(":'sad',
    ":)":'happy',
    ':D':"veryhappy",
    ":(":'upset',
    ':O':'scared'
  };
};