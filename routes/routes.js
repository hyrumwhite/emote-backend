/* global exports */
//var emotedb = require("../models/emote");
var parser = require("../lib/parser");
var posts = require("../models/posts");
var emotions = require("../models/emotions").emoticons();
exports.addReaction = posts.addReaction;
exports.createPost = function(req, res, broadcast) {
  var cbid = req.cbid;
  var content = req.body;
  var tags = parser.getTags(content);
  for(var emotion in emotions) {
    if(content.indexOf(emotion) !== -1) {
      tags.push('#' + emotions[emotion]);
    }
  }
  req.hashtags = tags;
  var post = posts.newPost(req);
  post.save();
  console.log("CREATING::");
  console.log(post);
  var response = {cbid:cbid, data:post};
  res.send(response);
  broadcast({broadcast:"postAdded", data:{}});
};
exports.addComment = function(req, res) {
  posts.addComment(req,res);
};
exports.likeComment = function(req, res) {
  posts.likeComment(req, res);
};
exports.getPost = function(req, res) {
  var id = req.id;
  var cbid = req.cbid;
  if(id !== undefined) {
    posts.getByQuery({_id:id}, function(error, post) {
      console.log('GETTING');
      console.log(post);
      var response = {cbid: cbid, data:post};
      res.send(response);
    });
  }
  else {
    posts.getAll(function(error, posts) {
      posts.cbid = cbid;
      if(error) {
        res.send(500 + "");
      }
      else {
        var response = {cbid: cbid, data:posts};
        res.send(response);
      }
    });
  }
};
exports.enterPool = function(req, res) {
  console.log("entering pool!");
  posts.enterPool(req,res);
};
exports.leavePool = function(req, res) {
  posts.leavePool(req,res);
};
exports.search = posts.search;
exports.getNextSet = posts.getNextSet;
