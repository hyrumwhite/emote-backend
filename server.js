var PORT = 3001;
var WebSocketServer = require('ws').Server;
var wss = new WebSocketServer({ port: PORT});
console.log('Listening on ' + PORT);
var routes = require('./routes/routes');

wss.broadcast = function broadcast(data) {
  for(var i = 0; i < wss.clients.length; i++) {
    wss.clients[i].send(JSON.stringify(data));
  }
};

//ws.send();
//wss.broadcast(test + "#");
wss.on('connection', function connection(ws) {
  ws.on('message', function incoming(message) {
    console.log('received: %s', message);
    var req = JSON.parse(message);
    var routeFunction = routes[req.endpoint];
    if(routeFunction !== undefined) {
      var res = {ws:ws};
      res.send = function(object) {
        var responseString = JSON.stringify(object);
        this.ws.send(responseString);
      };
      routeFunction(req, res, wss.broadcast);
    }
    else {
      ws.send("Endpoint does not exist");
    }
  });

});